var app = angular.module('app',[]);

app.controller("test", function($scope, tools, service) {
    let self = this;
    let glob = $scope;

    let url = "http://localhost/api";
    self.status = "";

    self.initial = function() {
        self.getData();
    }

    self.getData = () => {
        let promise = service.get(url, 1);
        promise.then(
            (response) => {
                console.log(response.data);
                if(response.data.status) {
                    self.images = response.data.data;
                }else {
                    self.status = response.data.err;
                    tools.show(".err");
                }
            }
        ).catch(
            () => {
                console.log("-- err --");
            }
        )
    }

    self.initial();

});

app.service('service', ['$http', '$q', function($http, $q) {
    var self = this;

    var deferObject, serviceMethods = {
        get: function(url,data) {
            var promise = $http.get(url, data),
                deferObject = deferObject || $q.defer();

            promise.then(
                function(answer) {
                    deferObject.resolve(answer);
                },
                function(reason) {
                    deferObject.reject(reason);
                }
            );
            return deferObject.promise;
        },

    };
    return serviceMethods;
}]);

app.service('tools', ['$http', '$q', function($http, $q) {
    let self = this;
    self.field = {
        qr:""
    }
    var deferObject, serviceMethods = {
        isActive: (element) => {
            $(element).addClass("is-active");
        },
        unActive: (element) => {
            $(element).removeClass("is-active");
        },
        hide: (element) => {
            $(element).addClass("is-hidden");
        },
        show: (element) => {
            $(element).removeClass("is-hidden");
        },
        modal: (element,mode="hidden") => {
            if(mode == "show") {
                $(element).addClass("is-active zoomIn");
            }else {
                $(element).removeClass("is-active zoomIn");
            }
        }
    }


    return serviceMethods;

    
}]);


app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});